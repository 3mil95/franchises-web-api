﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Models.Domain;

namespace WebAPI.Models
{
    public class FranchisesDbContext : DbContext
    {

        public DbSet<Movie> Movies { get; set; }
        public DbSet<Franchise> Franchises { get; set; }
        public DbSet<Character> Characters { get; set; }
        public DbSet<MovieCharacter> MovieCharacters { get; set; }


        public FranchisesDbContext([NotNullAttribute] DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MovieCharacter>().HasKey(mc => new { mc.CharcterId, mc.MoveId });


            modelBuilder.Entity<Franchise>().HasData(
                new Franchise { Id = 1, Name = "Star Wars", Description = "Star Wars" },
                new Franchise { Id = 2, Name = "The Lord of the Rings", Description = "The Lord of the Rings" }
            );


            modelBuilder.Entity<Movie>().HasData(
                new Movie { Id = 1, Title = "The Shawshank Redemption", Director = "Frank Darabont", Year = 1994, Genre = "Drama" },
                new Movie { Id = 2, Title = "The Fellowship of the Ring", Director = "Peter Jackson", Year = 2001, Genre = "Action", FranchiseId=2 },
                new Movie { Id = 3, Title = "The Two Towers", Director = "Peter Jackson", Year = 2002, Genre = "Action", FranchiseId = 2 },
                new Movie { Id = 4, Title = "The Return of the King", Director = "Peter Jackson", Year = 2002, Genre = "Action", FranchiseId = 2 },
                new Movie { Id = 5, Title = "Episode IX - The Rise of Skywalker", Director = "J.J.Abrams", Year = 2019, Genre = "Action", PictureUrl="url1233", FranchiseId = 1 }
            );

            modelBuilder.Entity<Character>().HasData(
                new Character { Id = 1, FullName = "Andy Dufresne", Gender = "Man", PictureUrl = "url1" },
                new Character { Id = 2, FullName = "Frodo", Gender = "Man", PictureUrl = "url2" },
                new Character { Id = 3, FullName = "Rey", Gender = "Woman", PictureUrl = "url2" }
            );

            modelBuilder.Entity<MovieCharacter>().HasData(
                new MovieCharacter { CharcterId = 1, MoveId = 1 },
                new MovieCharacter { CharcterId = 2, MoveId = 2 },
                new MovieCharacter { CharcterId = 2, MoveId = 3 },
                new MovieCharacter { CharcterId = 2, MoveId = 4 },
                new MovieCharacter { CharcterId = 3, MoveId = 5 }
            );

        }
    }
}
