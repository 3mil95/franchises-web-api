﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models.Domain
{
    public class MovieCharacter
    {

        [Key] 
        [Column(Order = 0)]
        public int MoveId { get; set; }

        [Key]
        [Column(Order = 1)]
        public int CharcterId { get; set; }

        [ForeignKey("MoveId")]
        public Movie Movie { get; set; }

        [ForeignKey("CharcterId")]
        public Character Character { get; set; }
    }
}
