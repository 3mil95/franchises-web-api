﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models.Domain
{
    public class Movie
    {

        public Movie()
        {
            MoveCharacters = new HashSet<MovieCharacter>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string Title { get; set; }

        [Required]
        [MaxLength(50)]
        public string Genre { get; set; }

        [Required]
        public int Year { get; set; }

        [Required]
        [MaxLength(50)]
        public string Director { get; set; }

        [MaxLength(50)]
        public string PictureUrl { get; set; }

        [MaxLength(50)]
        public string TrailerUrl { get; set; }

        public int? FranchiseId { get; set; }

        public Franchise Franchise { get; set; }

        public ICollection<MovieCharacter> MoveCharacters { get; set; }
    }
}
