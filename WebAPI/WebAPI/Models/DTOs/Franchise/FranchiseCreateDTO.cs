﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models.DTOs.Franchise
{
    public class FranchiseCreateDTO
    {
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
