﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using WebAPI.Models.DTOs.Movie;

namespace WebAPI.Models.DTOs.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {

            CreateMap<Domain.Movie, MovieEditDTO>().ReverseMap();

            CreateMap<Domain.Movie, MovieReadDTO>()
                .ForMember(adto => adto.Franchise, opt => opt
                .MapFrom(a => a.FranchiseId))
                .ReverseMap();

            CreateMap<Domain.Movie, MovieCreateDTO>().ReverseMap();

        }
    }
}
