﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using WebAPI.Models.DTOs.Character;

namespace WebAPI.Models.DTOs.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<Domain.Character, CharacterReadDTO>().ReverseMap();

            CreateMap<Domain.Character, CharacterEditDTO>().ReverseMap();

            CreateMap<Domain.Character, CharacterCreateDTO>().ReverseMap();
        }
    }
}
