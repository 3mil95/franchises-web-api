﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Models.Domain;
using WebAPI.Models.DTOs.Franchise;

namespace WebAPI.Models.DTOs.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Domain.Franchise, FranchiseReadDTO>().ReverseMap();

            CreateMap<Domain.Franchise, FranchiseCreateDTO>().ReverseMap();

            CreateMap<Domain.Franchise, FranchiseEditDTO>().ReverseMap();
        }

    }
}
