﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebAPI.Migrations
{
    public partial class SeedingDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Characters",
                columns: new[] { "Id", "Alias", "FullName", "Gender", "PictureUrl" },
                values: new object[,]
                {
                    { 1, null, "Andy Dufresne", "Man", "url1" },
                    { 2, null, "Frodo", "Man", "url2" },
                    { 3, null, "Rey", "Woman", "url2" }
                });

            migrationBuilder.InsertData(
                table: "Franchises",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "Star Wars", "Star Wars" },
                    { 2, "The Lord of the Rings", "The Lord of the Rings" }
                });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "PictureUrl", "Title", "TrailerUrl", "Year" },
                values: new object[] { 1, "Frank Darabont", null, "Drama", null, "The Shawshank Redemption", null, 1994 });

            migrationBuilder.InsertData(
                table: "MovieCharacters",
                columns: new[] { "CharcterId", "MoveId" },
                values: new object[] { 1, 1 });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "PictureUrl", "Title", "TrailerUrl", "Year" },
                values: new object[,]
                {
                    { 5, "J.J.Abrams", 1, "Action", "url1233", "Episode IX - The Rise of Skywalker", null, 2019 },
                    { 2, "Peter Jackson", 2, "Action", null, "The Fellowship of the Ring", null, 2001 },
                    { 3, "Peter Jackson", 2, "Action", null, "The Two Towers", null, 2002 },
                    { 4, "Peter Jackson", 2, "Action", null, "The Return of the King", null, 2002 }
                });

            migrationBuilder.InsertData(
                table: "MovieCharacters",
                columns: new[] { "CharcterId", "MoveId" },
                values: new object[,]
                {
                    { 3, 5 },
                    { 2, 2 },
                    { 2, 3 },
                    { 2, 4 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "MovieCharacters",
                keyColumns: new[] { "CharcterId", "MoveId" },
                keyValues: new object[] { 1, 1 });

            migrationBuilder.DeleteData(
                table: "MovieCharacters",
                keyColumns: new[] { "CharcterId", "MoveId" },
                keyValues: new object[] { 2, 2 });

            migrationBuilder.DeleteData(
                table: "MovieCharacters",
                keyColumns: new[] { "CharcterId", "MoveId" },
                keyValues: new object[] { 2, 3 });

            migrationBuilder.DeleteData(
                table: "MovieCharacters",
                keyColumns: new[] { "CharcterId", "MoveId" },
                keyValues: new object[] { 2, 4 });

            migrationBuilder.DeleteData(
                table: "MovieCharacters",
                keyColumns: new[] { "CharcterId", "MoveId" },
                keyValues: new object[] { 3, 5 });

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Franchises",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Franchises",
                keyColumn: "Id",
                keyValue: 2);
        }
    }
}
