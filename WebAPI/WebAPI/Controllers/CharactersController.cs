﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using WebAPI.Models;
using WebAPI.Models.Domain;
using WebAPI.Models.DTOs.Character;

namespace WebAPI.Controllers
{

    [Route("api/v1/characters")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class CharactersController : Controller
    {

        private readonly FranchisesDbContext _dbContext;
        private readonly IMapper _mapper;

        public CharactersController(FranchisesDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }




        /// <summary>
        /// Get all characters.
        /// </summary>
        /// <response code = "200">Returns all the characters.</response>
        /// <returns>A list with all the characters.</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters()
        {
            var characters = await _dbContext.Characters.ToListAsync();
            var charactersDTOs = _mapper.Map<List<CharacterReadDTO>>(characters);
            return Ok(charactersDTOs);
        }

        /// <summary>
        /// Get a character by its id.
        /// </summary>
        /// <param name="id">The id of the character you want to get.</param>
        /// <response code = "404">If there is no character with that id.</response>
        /// <response code = "200">Returns the character with the id specified.</response>
        /// <returns>A single franchise</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacter(int id)
        {
            var character = await _dbContext.Characters.FindAsync(id);

            if (character == null)
            {
                return NotFound();
            }

            var characterDTO = _mapper.Map<CharacterReadDTO>(character);
            return Ok(characterDTO);
        }

        /// <summary>
        /// Add a character.
        /// </summary>
        /// <param name="character">The character you want to add.</param>
        /// <response code = "201">Returns the newly created character.</response>
        /// <response code = "400">If there is something wrong with the character data.</response>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<CharacterReadDTO>> PostCharacter(CharacterCreateDTO character)
        {
            var domainCharacter = _mapper.Map<Character>(character);

            try
            {
                await _dbContext.AddAsync(domainCharacter);
                await _dbContext.SaveChangesAsync();
            }catch(DbUpdateException)
            {
                return BadRequest();
            }

            return CreatedAtAction("GetCharacter", new { id = domainCharacter.Id }, _mapper.Map<CharacterReadDTO>(domainCharacter));
        }

        /// <summary>
        /// Remove a character.
        /// </summary>
        /// <param name="id">The id of the character you want to remove.</param>
        /// <response code = "400">If character is not deletable due to e.g. that it is referenced in another table.</response>
        /// <response code = "404">If there is no character with that id.</response>
        /// <response code = "204">The character was deleted.</response>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> DeleteCharacter(int id)
        {
            var character = await _dbContext.Characters.FindAsync(id);

            if (character == null)
            {
                return NotFound();
            }

            try
            {
                _dbContext.Characters.Remove(character);
                await _dbContext.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                return BadRequest();
            }
            
            return NoContent();
        }

        /// <summary>
        /// Update all properties of a character.
        /// </summary>
        /// <param name="id">The id of the character to be updated.</param>
        /// <param name="character">The new state of the character.</param>
        /// <response code = "400">If there is something wrong with the character data.</response>
        /// <response code = "404">If there is no character with that id.</response>
        /// <response code = "204">The character was updated.</response>
        /// <returns></returns>
        [HttpPut("{id})")]
        public async Task<ActionResult> PutCharacter(int id, CharacterEditDTO character)
        {
            var domainCharacter = _mapper.Map<Character>(character);

            if (id != domainCharacter.Id)
            {
                return BadRequest();
            }

            try
            {
                _dbContext.Entry(domainCharacter).State = EntityState.Modified;
                await _dbContext.SaveChangesAsync();
            } catch (DbUpdateConcurrencyException)
            {
                if (!CharacterExists(id))
                {
                    return NotFound();
                }

                throw;
            }

            return NoContent();
        }

        private bool CharacterExists(int id)
        {
            return _dbContext.Characters.Any(e => e.Id == id);
        }

    }
}
