﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using WebAPI.Models;
using WebAPI.Models.Domain;
using WebAPI.Models.DTOs.Character;
using WebAPI.Models.DTOs.Movie;

namespace WebAPI.Controllers
{
    [Route("api/v1/movies")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class MoviesController : Controller
    {

        private readonly FranchisesDbContext _dbContext;
        private readonly IMapper _mapper;

        public MoviesController(FranchisesDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }




        /// <summary>
        /// Get all movies.
        /// </summary>
        /// <response code = "200">Returns all the movies.</response>
        /// <returns>A list with all the movies.</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies()
        {
            var movies = await _dbContext.Movies.ToListAsync();
            var moviesDTOs = _mapper.Map<List<MovieReadDTO>>(movies);
            return Ok(moviesDTOs);
        }

        /// <summary>
        /// Get a movie by its id.
        /// </summary>
        /// <param name="id">The id of the movie you want to get.</param>
        /// <response code = "404">If there is no movie with that id.</response>
        /// <response code = "200">Returns the movie with the id specified.</response>
        /// <returns>A single movie</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovie(int id)
        {
            var movie = await _dbContext.Movies.FindAsync(id);

            if (movie == null)
            {
                return NotFound();
            }

            var movieDTO = _mapper.Map<MovieReadDTO>(movie);
            return Ok(movieDTO);
        }

        /// <summary>
        /// Add a movie.
        /// </summary>
        /// <param name="movie">The movie you want to add.</param>
        /// <response code = "201">Returns the newly created movie.</response>
        /// <response code = "400">If there is something wrong with the movie data.</response>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<MovieReadDTO>> PostMovie(MovieCreateDTO movie)
        {
            var domainMovie = _mapper.Map<Movie>(movie);

            try
            {
                await _dbContext.AddAsync(domainMovie);
                await _dbContext.SaveChangesAsync();
            } catch(DbUpdateException)
            {
                return BadRequest();
            }

            return CreatedAtAction("GetMovie", new { id = domainMovie.Id }, _mapper.Map<MovieReadDTO>(domainMovie));
        }

        /// <summary>
        /// Remove a movie.
        /// </summary>
        /// <param name="id">The id of the movie you want to remove.</param>
        /// <response code = "400">If movie is not deletable due to e.g. that it is referenced in another table.</response>
        /// <response code = "404">If there is no movie with that id.</response>
        /// <response code = "204">The movie was deleted.</response>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> DeleteMovie(int id)
        {
            var movie = await _dbContext.Movies.FindAsync(id);

            if (movie == null)
            {
                return NotFound();
            }

            try
            {
                _dbContext.Movies.Remove(movie);
                await _dbContext.SaveChangesAsync();
            } catch(DbUpdateException)
            {
                return BadRequest();
            }


            return NoContent();
        }

        /// <summary>
        /// Update all properties of a movie.
        /// </summary>
        /// <param name="id">The id of the movie to be updated.</param>
        /// <param name="movie">The new state of the movie.</param>
        /// <response code = "400">If there is something wrong with the movie data.</response>
        /// <response code = "404">If there is no movie with that id.</response>
        /// <response code = "204">The movie was updated.</response>
        /// <returns></returns>
        [HttpPut("{id})")]
        public async Task<ActionResult> PutMovie(int id, MovieEditDTO movie)
        {
            var domainMovie = _mapper.Map<Movie>(movie);

            if (id != domainMovie.Id)
            {
                return BadRequest();
            }

            try
            {
                _dbContext.Entry(domainMovie).State = EntityState.Modified;
                await _dbContext.SaveChangesAsync(); ;
            }
            catch(DbUpdateConcurrencyException)
            {
                if (!MovieExists(id))
                {
                    return NotFound();
                }
                
                throw;
            }

            return NoContent();
        }

        private bool MovieExists(int id)
        {
            return _dbContext.Movies.Any(e => e.Id == id);
        }





        /// <summary>
        /// Get all characters in a movie.
        /// </summary>
        /// <param name="id">The id of the movie the characters are in.</param>
        /// <response code = "404">If there is no movie with that id.</response>
        /// <response code = "200">Returns a list with all the characters in the specified movie.</response>
        /// <returns></returns>
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharactersFromMovie(int id)
        {
            var movie = _dbContext.Movies.Where(m => m.Id == id);
            if (await movie.CountAsync() == 0)
            {
                return NotFound();
            }
            var characters = await movie.SelectMany(c => c.MoveCharacters).Select(mc => mc.Character).ToListAsync();
            var charactersDTOs = _mapper.Map<List<CharacterReadDTO>>(characters);
            return Ok(charactersDTOs);
        }





        /// <summary>
        /// Add a character to a movie.
        /// </summary>
        /// <param name="movieId">The id of the movie</param>
        /// <param name="characterId">The id of the character</param>
        /// <response code = "400">If there is something wrong with the given data.</response>
        /// <response code = "404">If the character or movie do not exist.</response>
        /// <response code = "204">The character is added to the movie.</response>
        /// <returns></returns>
        [HttpPost("{movieId}/add/character/{characterId}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> AddCharacterToMovie(int movieId, int characterId)
        {
            var movieCharacter = new MovieCharacter { CharcterId = characterId, MoveId = movieId };

            try
            {
                await _dbContext.MovieCharacters.AddAsync(movieCharacter);
                await _dbContext.SaveChangesAsync();
            } catch(DbUpdateException)
            {
                var character = await _dbContext.Characters.FindAsync(characterId);
                var movie = await _dbContext.Movies.FindAsync(movieId);

                if (movie == null || character == null)
                {
                    return NotFound();
                }
                return BadRequest();
            }

            return NoContent();
        }

        /// <summary>
        /// Remove a character to a movie.
        /// </summary>
        /// <param name="movieId">The id of the movie</param>
        /// <param name="characterId">The id of the character</param>
        /// <response code = "400">If there is something wrong with the given ids.</response>
        /// <response code = "404">If the character is not in the movie.</response>
        /// <response code = "204">The character is removed from the movie.</response>
        /// <returns></returns>
        [HttpDelete("{movieId}/remove/character/{characterId}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> DeleteCharacterFromMovie(int movieId, int characterId)
        {
            var movieCharacter = await _dbContext.MovieCharacters.Where(mc => mc.CharcterId == characterId && mc.MoveId == movieId).FirstOrDefaultAsync();
            if (movieCharacter == null)
            {
                return NotFound();
            }
            try
            {
                _dbContext.MovieCharacters.Remove(movieCharacter);
                await _dbContext.SaveChangesAsync();
            }
            catch(DbUpdateException)
            {
                return BadRequest();
            }

            return NoContent();
        }



    }
}
