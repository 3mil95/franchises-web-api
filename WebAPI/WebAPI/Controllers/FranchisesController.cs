﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using WebAPI.Models;
using WebAPI.Models.Domain;
using WebAPI.Models.DTOs.Character;
using WebAPI.Models.DTOs.Franchise;
using WebAPI.Models.DTOs.Movie;

namespace WebAPI.Controllers
{
    [Route("api/v1/franchises")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class FranchisesController : ControllerBase
    {
        private readonly FranchisesDbContext _dbContext;
        private readonly IMapper _mapper;

        public FranchisesController(FranchisesDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all franchises.
        /// </summary>
        /// <response code = "200">Returns all the franchises.</response>
        /// <returns>A list with all the franchises</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises()
        {
            var franchises = await _dbContext.Franchises.ToListAsync();
            var franchisesDTOs = _mapper.Map<List<FranchiseReadDTO>>(franchises);
            return Ok(franchisesDTOs);
        }

        /// <summary>
        /// Get a franchise by its id.
        /// </summary>
        /// <param name="id">The id of the franchise you want to get.</param>
        /// <response code = "404">If there is no franchise with that id.</response>
        /// <response code = "200">Returns the franchise with the id specified.</response>
        /// <returns>A single franchise</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int id)
        {
            var franchise = await _dbContext.Franchises.FindAsync(id);

            if (franchise == null)
            {
                return NotFound();
            }

            var franchiseDTO = _mapper.Map<FranchiseReadDTO>(franchise);
            return Ok(franchiseDTO);
        }

        /// <summary>
        /// Add a franchise.
        /// </summary>
        /// <param name="franchise">The franchise you want to add.</param>
        /// <response code = "201">Returns the newly created franchise.</response>
        /// <response code = "400">If there is something wrong with the franchise data.</response>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<FranchiseReadDTO>> PostFranchise(FranchiseCreateDTO franchise)
        {
            var domainFranchise = _mapper.Map<Franchise>(franchise);

            try
            {
                await _dbContext.AddAsync(domainFranchise);
                await _dbContext.SaveChangesAsync();
            }
            catch(DbUpdateException)
            {
                return BadRequest();
            }

            return CreatedAtAction("GetFranchise", new { id = domainFranchise.Id }, _mapper.Map<FranchiseReadDTO>(domainFranchise));
        }

        /// <summary>
        /// Remove a franchise.
        /// </summary>
        /// <param name="id">The id of the franchise you want to remove.</param>
        /// <response code = "400">If franchise is not deletable due to e.g. that it is referenced in another table.</response>
        /// <response code = "404">If there is no franchise with that id.</response>
        /// <response code = "204">The franchise was deleted.</response>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> DeleteFranchise(int id)
        {
            var franchise = await _dbContext.Franchises.FindAsync(id);

            if (franchise == null)
            {
                return NotFound();
            }

            try
            {
                _dbContext.Franchises.Remove(franchise);
                await _dbContext.SaveChangesAsync();
            } catch(DbUpdateException)
            {
                return BadRequest();
            }
            return NoContent();
        }


        /// <summary>
        /// Update all properties of a franchise.
        /// </summary>
        /// <param name="id">The id of the franchise to be updated.</param>
        /// <param name="franchise">The new state of the franchise.</param>
        /// <response code = "400">If there is something wrong with the franchise data.</response>
        /// <response code = "404">If there is no franchise with that id.</response>
        /// <response code = "204">The franchise was updated.</response>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<ActionResult> PutFranchise(int id, FranchiseEditDTO franchise)
        {
            var domainFranchise = _mapper.Map<Franchise>(franchise);

            if (id != domainFranchise.Id)
            {
                return BadRequest();
            }

            try
            {
                _dbContext.Entry(domainFranchise).State = EntityState.Modified;
                await _dbContext.SaveChangesAsync();
            } catch(DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }

                throw;
            }

            return NoContent();
        }

        private bool FranchiseExists(int id)
        {
            return _dbContext.Franchises.Any(e => e.Id == id);
        }





        /// <summary>
        /// Get all movies in a franchise.
        /// </summary>
        /// <param name="id">The id of the franchise the movies are in.</param>
        /// <response code = "404">If there is no movie with that id.</response>
        /// <response code = "200">Returns a list with all the movies in the specified franchise.</response>
        /// <returns></returns>
        [HttpGet("{id}/movies")]
        public async Task<ActionResult<MovieReadDTO>> GetMoviesFromFranchise(int id)
        {
            var franchise = _dbContext.Franchises.Where(f => f.Id == id);
            if (await franchise.CountAsync() == 0)
            {
                return NotFound();
            }
            var movies = await franchise.SelectMany(f => f.Movies).ToListAsync();
            var moviesDTOs = _mapper.Map<List<MovieReadDTO>>(movies);
            return Ok(moviesDTOs);
        }

        /// <summary>
        /// Get all characters in a franchise.
        /// </summary>
        /// <param name="id">The id of the franchise the characters are in.</param>
        /// <response code = "404">If there is no franchise with that id.</response>
        /// <response code = "200">Returns a list with all the characters in the specified franchise.</response>
        /// <returns></returns>
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharactersFromFranchise(int id)
        {
            var franchise = _dbContext.Franchises.Where(f => f.Id == id);
            if (await franchise.CountAsync() == 0)
            {
                return NotFound();
            }
            var characters = await franchise.SelectMany(f => f.Movies).SelectMany(c => c.MoveCharacters).Select(mc => mc.Character).Distinct().ToArrayAsync();
            var charactersDTOs = _mapper.Map<List<CharacterReadDTO>>(characters);
            return Ok(charactersDTOs);
        }




        /// <summary>
        /// Add a movie to a franchise.
        /// </summary>
        /// <param name="franchiseId">The id of the franchise to be added to.</param>
        /// <param name="movieId">The id of the movie to be added.</param>
        /// <response code = "400">If there is something wrong with the given ids.</response>
        /// <response code = "404">If there is no movie with that id.</response>
        /// <response code = "204">The movie is added to the franchise.</response>
        /// <returns></returns>
        [HttpPatch("{franchiseId}/add/movie/{movieId}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> AddMoveToFranchise(int franchiseId, int movieId)
        {
            var movie = await _dbContext.Movies.FindAsync(movieId);
            if (movie == null)
            {
                return NotFound();
            }

            try
            {
                movie.FranchiseId = franchiseId;
                await _dbContext.SaveChangesAsync();
            } catch(DbUpdateException)
            {
                var franchise = await _dbContext.Franchises.FindAsync(franchiseId);
                if (franchise == null)
                {
                    return NotFound();
                }
                return BadRequest();
            }

            return NoContent();
        }

        /// <summary>
        /// Remove a movie from its a franchise.
        /// </summary>
        /// <param name="movieId">The id of movie to be removed from franchise</param>
        /// <response code = "400">If there is something wrong with the given id.</response>
        /// <response code = "404">If there is no movie with that id.</response>
        /// <response code = "204">The movie is no longer part of a franchise.</response>
        /// <returns></returns>
        [HttpPatch("remove/movie/{movieId}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> RemoveMoveToFranchise(int movieId)
        {
            var movie = await _dbContext.Movies.FindAsync(movieId);
            if (movie == null)
            {
                return NotFound();
            }

            try
            {
                movie.FranchiseId = null;
                await _dbContext.SaveChangesAsync();
            }
            catch(DbUpdateException)
            {
                return BadRequest();
            }

            return NoContent();
        }



    }
}
